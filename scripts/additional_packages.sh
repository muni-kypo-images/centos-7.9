# check for updates
sudo yum update -y
#sudo dnf update -y

# install python3
sudo yum install python3 -y

# install console web browser
# sudo yum --enablerepo=PowerTools install elinks -y

# reinstall CA certificates
sudo yum reinstall -y ca-certificates

# Changelog

## [qemu-0.1.2] - 2022-01-29
### Fixed
- Hostname is added on boot to /etc/hosts

## [qemu-0.1.1], [vbox-0.1.1] - 2021-09-29
### Fixed
- SSL CA certificates

## [qemu-0.1.0], [vbox-0.1.0] - 2021-06-22
### Added
- Initial version

[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-7.9/-/tree/qemu-0.1.0
[vbox-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-7.9/-/tree/vbox-0.1.0
[qemu-0.1.1]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-7.9/-/tree/qemu-0.1.1
[vbox-0.1.1]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-7.9/-/tree/vbox-0.1.1
[qemu-0.1.2]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-7.9/-/tree/qemu-0.1.2
